## Crontab

PHP standalone library to ease the generation of a crontab string

## Installation

```
composer require appyourself/crontab
```

## Usage

```php
// Example
// In a crontab.php file
require_once __DIR__ . '/vendor/autoload.php';

use Appyourself\CronTab\CronCommand as Cmd;
use Appyourself\CronTab\CronJob as Cron;
use Appyourself\CronTab\CronScheduleString as Schedule;
use Appyourself\CronTab\CronTab;

$cronTab = new CronTab();
echo $cronTab->addJob(Cron::create(Schedule::everyMinutes(5), Cmd::fromFolder('/path/to/job1', 'job1')))
             ->addJob(Cron::create(Schedule::dailyAt('12:34'), Cmd::noOutput('job2'), 'comment'))
             ->dump();
             
// Will produce
// */5 * * * * cd /path/to/job1 && job1
// # comment
// 34 12 * * * job2 > /dev/null 2>&1
```