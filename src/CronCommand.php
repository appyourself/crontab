<?php

namespace Appyourself\CronTab;

class CronCommand
{
    public static function fromFolder($folder, $cmd): string
    {
        return sprintf('cd %s && %s', $folder, $cmd);
    }

    public static function noOutput($cmd): string
    {
        return sprintf('%s > /dev/null 2>&1', $cmd);
    }
}
