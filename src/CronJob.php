<?php

namespace Appyourself\CronTab;

class CronJob
{
    public $scheduleString;

    public $cmd;

    public $comment;

    public static function create($scheduleString, $cmd, $comment = null): CronJob
    {
        $cron = new self();

        $cron->scheduleString = $scheduleString;
        $cron->cmd            = $cmd;
        $cron->comment        = $comment;

        return $cron;
    }
}
