<?php

namespace Appyourself\CronTab;

/**
 *   *    *    *    *    *    *
 *   -    -    -    -    -    -
 *   |    |    |    |    |    |
 *   |    |    |    |    |    + year [optional]
 *   |    |    |    |    +----- day of week (0 - 7) (Sunday=0 or 7)
 *   |    |    |    +---------- month (1 - 12)
 *   |    |    +--------------- day of month (1 - 31)
 *   |    +-------------------- hour (0 - 23)
 *   +------------------------- min (0 - 59)
 */
class CronScheduleString
{
    public $min        = '*';

    public $hour       = '*';

    public $dayOfMonth = '*';

    public $month      = '*';

    public $dayOfWeek  = '*';

    public $year       = '*';

    /**
     * @param int $minutes
     *
     * @return CronScheduleString
     *
     * @throws \OutOfRangeException
     */
    public static function everyMinutes($minutes): CronScheduleString
    {
        $schedule = new self();

        $schedule->min = self::every(self::guardRange($minutes, 1, 59));

        return $schedule;
    }

    /**
     * @param int $hours
     * @param int $atMinute
     *
     * @return CronScheduleString
     *
     * @throws \OutOfRangeException
     */
    public static function everyHours($hours, $atMinute = null): CronScheduleString
    {
        $schedule = new self();

        $schedule->min  = $atMinute ?? self::getRandomMinute();
        $schedule->hour = self::every(self::guardRange($hours, 1, 23));

        return $schedule;
    }

    /**
     * @param string $simplifiedTimeString , ex : "12:34"
     *
     * @return CronScheduleString
     *
     * @throws \InvalidArgumentException when simplifiedTimeString cannot be parsed
     */
    public static function everyFirstDayOfMonth($simplifiedTimeString): CronScheduleString
    {
        $schedule = new self();

        $time = self::guardResolvableTime($simplifiedTimeString);

        $schedule->min        = (int)$time->format('i');
        $schedule->hour       = (int)$time->format('G');
        $schedule->dayOfMonth = 1;

        return $schedule;
    }

    /**
     * @param string $simplifiedTimeString , ex : "12:34"
     *
     * @return CronScheduleString
     *
     * @throws \InvalidArgumentException when simplifiedTimeString cannot be parsed
     */
    public static function dailyAt($simplifiedTimeString): CronScheduleString
    {
        $schedule = new self();

        $time = self::guardResolvableTime($simplifiedTimeString);

        $schedule->min  = (int)$time->format('i');
        $schedule->hour = (int)$time->format('G');

        return $schedule;
    }

    private static function getRandomMinute(): int
    {
        try {
            return random_int(0, 59);
        } catch (\Exception $e) {
        }

        return 0;
    }

    public function __toString()
    {
        return sprintf(
            '%s %s %s %s %s',
            $this->min,
            $this->hour,
            $this->dayOfMonth,
            $this->month,
            $this->dayOfWeek
        );
    }

    private static function every($timePart): string
    {
        return sprintf('*/%d', $timePart);
    }

    /**
     * @param int $number
     * @param int $min
     * @param int $max
     *
     * @return int
     *
     * @throws \OutOfRangeException
     */
    private static function guardRange($number, $min, $max): int
    {
        $number = (int)$number;

        if ($number < $min || $number > $max) {
            throw new \OutOfRangeException(sprintf('Valid input is between %d and %d', $min, $max));
        }

        return $number;
    }

    /**
     * @param string $simplifiedTimeString
     *
     * @return \DateTime
     *
     * @throws \InvalidArgumentException
     */
    private static function guardResolvableTime($simplifiedTimeString): \DateTime
    {
        if (!($time = \DateTime::createFromFormat('H:i', $simplifiedTimeString))) {
            throw new \InvalidArgumentException('Simplified time string given cannot be resolved');
        }

        return $time;
    }
}
