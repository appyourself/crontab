<?php

namespace Appyourself\CronTab;

class CronTab
{
    /**
     * @var CronJob[]
     */
    private $jobs;

    private $dumped;

    public function addJob(CronJob $job)
    {
        $this->jobs[] = $job;

        return $this;
    }

    public function dump($nbMarginLines = 0)
    {
        $this->start($nbMarginLines);

        $this->writeJobs();

        return $this->end($nbMarginLines);
    }

    private function start($nbMarginLines): void
    {
        $this->dumped = '';

        $this->addMargins($nbMarginLines);
    }

    private function writeJobs(): void
    {
        foreach ($this->jobs as $job) {
            $this->writeJob($job);
        }
    }

    private function end($nbMarginLines)
    {
        $this->addMargins($nbMarginLines);

        return $this->dumped;
    }

    private function addMargins($nbMarginLines): void
    {
        for ($i = 0; $i < $nbMarginLines; $i++) {
            $this->writeNewLine();
        }
    }

    private function writeJob(CronJob $job): void
    {
        if ($comment = $job->comment) {
            $this->writeComment($comment);
        }

        $this->writeScheduledCmd((string)$job->scheduleString, (string)$job->cmd);
    }

    private function writeComment($comment): void
    {
        foreach (explode("\n", $comment) as $commentLine) {
            $this->writeLine(sprintf('# ' . $commentLine));
        }
    }

    private function writeScheduledCmd($scheduleString, $cmd): void
    {
        $this->writeLine(sprintf('%s %s', $scheduleString, $cmd));
    }

    private function writeLine($string): void
    {
        $this->dumped .= $string;

        $this->writeNewLine();
    }

    private function writeNewLine(): void
    {
        $this->dumped .= "\n";
    }
}
