<?php

namespace Appyourself\CronTab\Test\Integration;

use Appyourself\CronTab\CronCommand as Cmd;
use Appyourself\CronTab\CronJob as Cron;
use Appyourself\CronTab\CronScheduleString as Schedule;
use Appyourself\CronTab\CronTab;
use PHPUnit\Framework\TestCase;

class FullCronTabTest extends TestCase
{
    /**
     * @test
     */
    public function canGenerateFullCronTab()
    {
        $cronTab = new CronTab();
        $cronTab->addJob(Cron::create(Schedule::everyMinutes(5), Cmd::fromFolder('/path/to/job1', 'job1')))
                ->addJob(Cron::create(Schedule::dailyAt('12:34'), Cmd::noOutput('job2'), 'comment'));

        $this->assertEquals(
            '
*/5 * * * * cd /path/to/job1 && job1
# comment
34 12 * * * job2 > /dev/null 2>&1

', $cronTab->dump(1)
        );
    }
}
