<?php

namespace Appyourself\Cron\Test\Unit;

use Appyourself\CronTab\CronJob;
use Appyourself\CronTab\CronTab;
use PHPUnit\Framework\TestCase;

class CronTabTest extends TestCase
{
    /**
     * @var CronTab
     */
    private $crontab;

    protected function setUp(): void
    {
        $this->crontab = new CronTab();
    }

    /**
     * @test
     */
    public function canDumpSingleJob()
    {
        $this->addJob('12:34', 'job1');

        $this->assertCronTabIs(
            '12:34 job1
'
        );
    }

    /**
     * @test
     */
    public function canDumpMultipleCommentedJobs()
    {
        $this->addJob('12:34', 'job1')
             ->addJob('12:35', 'job2', 'comment')
             ->addJob('12:36', 'job3');

        $this->assertCronTabIs(
            '12:34 job1
# comment
12:35 job2
12:36 job3
'
        );
    }

    /**
     * @test
     */
    public function canDumpMultilineCommentedJob()
    {
        $this->addJob('12:34', 'job1', "multiline\ncomment");

        $this->assertCronTabIs(
            '# multiline
# comment
12:34 job1
'
        );
    }

    /**
     * @test
     */
    public function canAddNewLineMargins()
    {
        $this->addJob('12:34', 'job1');

        $this->assertCronTabIs(
            '

12:34 job1


', 2
        );
    }

    private function addJob($scheduleStr, $cmd, $comment = null)
    {
        $this->crontab->addJob(CronJob::create($scheduleStr, $cmd, $comment));

        return $this;
    }

    private function assertCronTabIs($expected, $nbNewLineMargins = 0)
    {
        $this->assertEquals($expected, $this->crontab->dump($nbNewLineMargins));
    }
}
