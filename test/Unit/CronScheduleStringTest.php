<?php

namespace Appyourself\CronTab\Test\Unit;

use Appyourself\CronTab\CronScheduleString;
use PHPUnit\Framework\TestCase;

class CronScheduleStringTest extends TestCase
{
    /**
     * @test
     * @expectedException \OutOfRangeException
     */
    public function canNotCreateCronScheduleStringWithInvalidMinute()
    {
	$this->expectException(\OutOfRangeException::class);
        CronScheduleString::everyMinutes(120);
    }

    /**
     * @test
     */
    public function canCreateEveryMinutesCronScheduleString()
    {
        $this->assertEquals('*/10 * * * *', (string)CronScheduleString::everyMinutes(10));
    }

    /**
     * @test
     * @expectedException \OutOfRangeException
     */
    public function canNotCreateCronScheduleStringWithInvalidHour()
    {
        $this->expectException(\OutOfRangeException::class);
        CronScheduleString::everyHours(36);
    }

    /**
     * @test
     */
    public function canCreateEveryHoursCronScheduleString()
    {
        $this->assertEquals('0 */2 * * *', (string)CronScheduleString::everyHours(2, 0));
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function whenSimplifiedTimeStringIsInvalidThenThrowException()
    {
	    $this->expectException(\InvalidArgumentException::class);
	    CronScheduleString::dailyAt('not a resolvable time');
    }

    /**
     * @test
     */
    public function canCreateDailyAtCronScheduleString()
    {
        $this->assertEquals('5 2 * * *', (string)CronScheduleString::dailyAt('02:05'));
    }

    /**
     * @test
     */
    public function canCreateMontlyAtCronScheduleString()
    {
        $this->assertEquals('15 0 1 * *', (string)CronScheduleString::everyFirstDayOfMonth('00:15'));
    }
}
