<?php

namespace Appyourself\CronTab\Test\Unit;

use Appyourself\CronTab\CronCommand;
use PHPUnit\Framework\TestCase;

class CronCommandTest extends TestCase
{
    /**
     * @test
     */
    public function canCreateFromFolderCronCommand()
    {
        $this->assertEquals('cd /test/folder && do-the-job', CronCommand::fromFolder('/test/folder', 'do-the-job'));
    }

    /**
     * @test
     */
    public function canCreateNoOutputCronCommand()
    {
        $this->assertEquals('do-the-job > /dev/null 2>&1', CronCommand::noOutput('do-the-job'));
    }
}
